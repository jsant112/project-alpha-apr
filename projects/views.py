from django.shortcuts import get_object_or_404, render, redirect
from projects.models import Project, ProjectForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def project_view(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "project.html", context)


@login_required
def show_project_detail(request, id):
    project = get_object_or_404(Project, id=id)
    project = Project.objects.get(id=id)
    context = {"project": project}
    return render(request, "details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            create = form.save(False)
            create.owner = request.user
            create.save()
            context = {"create": create}
            return redirect("list_projects")

    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "create.html", context)
