from django.urls import path
from .views import project_view, show_project_detail, create_project

urlpatterns = [
    path("", project_view, name="list_projects"),
    path("<int:id>/", show_project_detail, name="show_project"),
    path("create/", create_project, name="create_project"),
]
