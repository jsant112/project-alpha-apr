from django.shortcuts import redirect, render
from tasks.models import CreateTaskModel, Task
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskModel(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()
            return redirect("list_projects")
    else:
        form = CreateTaskModel()
    context = {
        "form": form,
    }
    return render(request, "createtask.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "showtasks.html", context)
