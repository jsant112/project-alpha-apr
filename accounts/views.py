from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from accounts.models import Loginmethod, Signupmethod


# Create your views here.
def login_view(request):
    if request.method == "POST":
        form = Loginmethod(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("list_projects")
    else:
        form = Loginmethod()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def logout_view(request):
    logout(request)
    return redirect("login")


def user_signup(request):
    if request.method == "POST":
        form = Signupmethod(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            confirm = form.cleaned_data["password_confirmation"]
            if password == confirm:
                user = User.objects.create_user(username, password=password)
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("password", "the passwords do not match")
    else:
        form = Signupmethod()
    context = {
        "form": form,
    }
    return render(request, "accounts/registration/signup.html", context)
