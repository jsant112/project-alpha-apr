from django.urls import path
from .views import login_view, logout_view, user_signup

urlpatterns = [
    path("login/", login_view, name="login"),
    path("logout/", logout_view, name="logout"),
    path("signup/", user_signup, name="signup"),
]
